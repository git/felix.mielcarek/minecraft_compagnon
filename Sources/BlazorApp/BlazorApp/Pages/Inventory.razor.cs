﻿using BlazorApp.Components;
using BlazorApp.Models;
using BlazorApp.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace BlazorApp.Pages;
    public partial class Inventory
    {
        [Inject]
        public IDataService DataService { get; set; }

        private int totalItem;
        
        [Inject]
        public IStringLocalizer<List> Localizer { get; set; }


        private string? title;
        public List<Item> Items { get; set; } = new List<Item>();

        /// <summary>
        /// first render initializ the item list
        /// </summary>
        /// <param name="firstRender"></param>
        /// <returns></returns>
        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }
            Items = await DataService.List(0, await DataService.Count());

            StateHasChanged();
        }
    }

