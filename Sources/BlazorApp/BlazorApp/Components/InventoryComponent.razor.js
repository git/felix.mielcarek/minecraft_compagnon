﻿window.Inventory =
{
    AddActions: function (data) {

        data.forEach(element => {
            var div = document.createElement('div');
            div.innerHTML = 'Aaction: ' + element.action + ' - Index: ' + element.index;

            if (element.item) {
                div.innerHTML += ' - Item Name: ' + element.item.name;
            }

            document.getElementById('actions').appendChild(div);
        });
    }
}