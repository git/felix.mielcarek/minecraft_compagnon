﻿using BlazorApp.Models;
using BlazorApp.Pages;
using Blazorise;
using Microsoft.AspNetCore.Components;
using System.Text.Json;

namespace BlazorApp.Components
{
    public partial class InventoryItem
    {
        [Parameter]
        public int Index { get; set; }

        [Parameter]
        public Item Item { get; set; }

        [Parameter]
        public bool NoDrop { get; set; }

        [CascadingParameter]
        public InventoryComponent Parent { get; set; }


        /// <summary>
        /// method call when a drag enter a slot and send an action
        /// </summary>
        internal void OnDragEnter()
        {
            if (NoDrop)
            {
                return;
            }

            Parent.Actions.Add(new InventoryAction { Action = "Drag Enter", Item = this.Item, Index = this.Index });
        }
        /// <summary>
        /// method call when a drag leave a slot and send an action
        /// </summary>
        internal void OnDragLeave()
        {
            if (NoDrop)
            {
                return;
            }

            Parent.Actions.Add(new InventoryAction { Action = "Drag Leave", Item = this.Item, Index = this.Index });
        }
        /// <summary>
        /// method that manage a drop and send an action
        /// </summary>
        internal void OnDrop()
        {
            
            if (NoDrop == true || Parent.CurrentDragItem == null)
            {
                return;
            }
            if(this.Item == null)
            {
                this.Item = ItemFactory.Create(Parent.CurrentDragItem);
                Parent.RecipeItems[this.Index] = this.Item;
                Parent.Actions.Add(new InventoryAction { Action = "Drop", Item = this.Item, Index = this.Index });
                if (Parent.CurrentDragItemIndex != -1)
                {
                    Parent.CurrentDragItem = null;
                    Parent.RecipeItems[Parent.CurrentDragItemIndex] = null;
                    Parent.Actions.Add(new InventoryAction { Action = "Moove", Item = this.Item, Index = this.Index });
                }

            }
            else if(this.Item.Id == Parent.CurrentDragItem.Id && Parent.CurrentDragItemIndex != this.Index)
            {
                if(this.Item.StackSize > this.Item.Num){
                    ItemFactory.Add(this.Item, Parent.CurrentDragItem);
                    Parent.RecipeItems[this.Index] = this.Item;
                    Parent.Actions.Add(new InventoryAction { Action = "Drop", Item = this.Item, Index = this.Index });
                    Parent.Actions.Add(new InventoryAction { Action = "Moove", Item = this.Item, Index = this.Index });
                }
                
            }
            else if(Parent.CurrentDragItemIndex != -1)
            {
                Parent.RecipeItems[Parent.CurrentDragItemIndex] = this.Item;
                this.Item = ItemFactory.Create(Parent.CurrentDragItem);
                Parent.RecipeItems[this.Index] = this.Item;
                
                Parent.Actions.Add(new InventoryAction { Action = "Swap Position", Item = this.Item, Index = this.Index });
            }

            Parent.RecipeItems[this.Index] = this.Item;
            string fileName = "Inventory.json";
            string jsonString = JsonSerializer.Serialize(Parent.RecipeItems);
            File.WriteAllText(fileName, jsonString);


            
            
            
        }
        /// <summary>
        /// method call when darg start and send an action
        /// </summary>

        private void OnDragStart()
        {
            
            Parent.CurrentDragItem = this.Item;
            Parent.CurrentDragItemIndex = this.Index;

            Parent.Actions.Add(new InventoryAction { Action = "Drag Start", Item = this.Item, Index = this.Index });
        }
        /// <summary>
        /// method call when drag end and send an action specialy when outside the inventory
        /// </summary>
        private void OnDragEnd()
        {
            if (Parent.Actions.Last().Action == "Drag Leave" && Parent.CurrentDragItemIndex != -1 && this.Item != null )
            {
                this.Item = null;
                Parent.Actions.Add(new InventoryAction { Action = "Delete", Item = this.Item, Index = this.Index });

                Parent.RecipeItems[this.Index] = null;
                string fileName = "Inventoy.json";
                string jsonString = JsonSerializer.Serialize(Parent.RecipeItems);
                File.WriteAllText(fileName, jsonString);
            }

            if (Parent.Actions.Last().Action == "Moove" && Parent.CurrentDragItemIndex != -1)
            {
                this.Item = null;
                Parent.RecipeItems[this.Index] = null;
                string fileName = "Inventory.json";
                string jsonString = JsonSerializer.Serialize(Parent.RecipeItems);
                File.WriteAllText(fileName, jsonString);
            }

            if (Parent.Actions.Last().Action == "Swap Position") // to refresh the displayed item
            {
                this.Item = Parent.RecipeItems[this.Index];
            }

        }
    }
}
