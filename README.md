# Projet création inventaire Minecraft en Blazor

Projet dont l'énoncé est disponible sur le lien suivant: https://codefirst.iut.uca.fr/documentation/julien.riboulet/docusaurus/Blazor/fr/your-next-steps/

## Tuto pour lancer le projet

Le projet est disponible sur la branche principale.

Afin de pouvoir utiliser notre solution il vous faudra une fois cloner parametrer le lancement de notre projet et de l'api.
Pour ce faire:

    1. Cliquez droit sur la solution 'BlazorApp'
    2. Proriétés
![Image clique droit](/Documentation/settingsProject.png)
    
    3. Il vous faut maintenant définir l'action des 2 solutions sur 'Démarrage'


![Image menu propriétés](/Documentation/Settings.png)

    5. Pour finir démarrer le porjet avec le bouton 'Démarrer'
    
![Image démarrage](/Documentation/StartBar.png)

Ce projet est réalisé par Lucas Delanier et Félix Mielcarek.